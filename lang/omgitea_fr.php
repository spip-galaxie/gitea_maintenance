<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A

	// C

	// D
	'dashboard_menu'       => 'Dashboard Gitea SPIP',
	'dashboard_gitea_nom'  => 'Dashboard de la forge Gitea de SPIP',
	'dashboard_gitea_desc' => 'Le dashboard de la forge Gitea de SPIP permet de contrôler la cohérence des organisations au fil du temps. Ce dashboard utilise les objets et les mécanismes du plugin Check Factory.',

	// D
	'groupe_gitea_user_nom' => 'Utilisateurs',
	'groupe_gitea_orga_nom' => 'Organisations',
	'groupe_gitea_repo_nom' => 'Dépôts',

	// L

	// P
	'page_configurer_bouton' => 'Configurer le dashboard',
	'page_configurer_titre'  => 'Configuration du dashboard de Gitea',

	// R
	'repo_organisation_titre' => 'Fichiers des repos des organisations Gitea',
	'repo_organisation_sinon' => 'Aucun fichier disponible',

	// T
	'token_api_label'                => 'Token d\'autorisation d\'accès à l\'API Gitea',
	'type_controle_user_recent_nom'  => 'Utilisateurs récemment inscrits',
	'type_controle_orga_repos_nom'   => 'Liste des repos des organisations',
	'type_controle_user_recent_desc' => 'Ce contrôle permet de récupérer la liste des users Gitea dans un fichier JSON téléchargeable et affiche la liste des n derniers inscrits, n pouvant être choisi à chaque exécution.',
	'type_controle_orga_repos_desc'  => 'Ce contrôle génère, pour chaque organisation Gitea, un fichier téléchargeable de la liste des repos sous la forme organisation/repo.',

	// U
	'user_recent_titre'    => '@nb@ derniers users Gitea inscrits',
	'user_gitea_titre'     => 'Fichier JSON de tous les users Gitea',
	'user_gitea_sinon'     => 'Fichier JSON non disponible',
	'user_nb_recent_label' => 'Choisir le nombre de users à afficher',
);
