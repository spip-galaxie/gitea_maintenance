<?php
/**
 * Ce fichier contient l'ensemble des constantes et des utilitaires nécessaires au fonctionnement du service web Gitea.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_GITEA_SPIP_ENDPOINT_BASE_URL')) {
	/**
	 * Préfixe des URL du service web de Gitea SPIP.
	 */
	define('_GITEA_SPIP_ENDPOINT_BASE_URL', 'https://git.spip.net/api/v1/');
}

/**
 * Construit l'URL de la requête Gitea correspondant à la demande utilisateur.
 *
 * @param array $parametres Liste des paramètres de l'url à accoler dans l'ordre fourni.
 *
 * @return string L'URL de la requête au service
 * @internal
 *
 */
function gitea_construire_url($parametres = array()) {

	// Récupération du token d'autorisation d'accès à l'API
	include_spip('inc/config');
	$token = lire_config('omgitea/token');

	// Construire l'URL de l'api sollicitée
	$url = _GITEA_SPIP_ENDPOINT_BASE_URL
		   . implode('/', $parametres)
		   . "?access_token=${token}";

	return $url;
}

/**
 * Renvoie, à partir de l'url du service, le tableau des données demandées.
 * Le service utilise dans ce cas une chaine JSON qui est décodée pour fournir
 * le tableau de sortie. Le flux retourné par le service est systématiquement
 * transcodé dans le charset du site avant d'être décodé.
 *
 * @uses recuperer_url()
 *
 * @param string   $url        URL complète de la requête au service web concerné.
 * @param null|int $taille_max Taille maximale du flux récupéré suite à la requête.
 *                             `null` désigne la taille par défaut.
 *
 * @return array
 */
function gitea_requeter($url, $taille_max = null) {

	// Initialisation de la réponse
	$reponse = array(
		'donnees' => array(),
		'erreur'  => ''
	);

	// Acquisition des données spécifiées par l'url
	include_spip('inc/distant');
	$options = array(
		'transcoder' => true,
		'taille_max' => $taille_max);
	$flux = recuperer_url($url, $options);

	$reponse = array();
	if (empty($flux['page'])) {
		spip_log("URL indiponible : ${url}", 'omgitea');
		$reponse['erreur'] = 'url_indisponible';
	} else {
		// Transformation de la chaîne json reçue en tableau associatif
		try {
			$reponse['donnees'] = json_decode($flux['page'], true);
		} catch (Exception $erreur) {
			$reponse['erreur'] = 'analyse_json';
			spip_log("Erreur d'analyse JSON pour l'URL `${url}` : " . $erreur->getMessage(), 'omgitea' . _LOG_ERREUR);
		}
	}

	return $reponse;
}
