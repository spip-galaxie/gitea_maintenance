<?php
/**
 * Ce fichier contient les fonctions de service nécessité par le plugin Check Factory.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la configuration spécifique des caches de Gitea maintenance.
 *
 * @param string $plugin Identifiant qui permet de distinguer le module appelant qui peut-être un plugin comme le noiZetier ou
 *                       un script. Pour un plugin, le plus pertinent est d'utiliser le préfixe.
 *
 * @return array Tableau de la configuration brute du plugin.
 */
function omgitea_cache_configurer($plugin) {

	// Initialisation du tableau de configuration avec les valeurs par défaut du plugin Check Factory.
	$configuration = array(
		'repo' => array(
			'racine'          => '_DIR_VAR',
			'sous_dossier'    => false,
			'nom_prefixe'     => 'repo',
			'nom_obligatoire' => array('organisation'),
			'nom_facultatif'  => array(),
			'extension'       => '.txt',
			'securisation'    => false,
			'serialisation'   => false,
			'decodage'        => false,
			'separateur'      => '_',
			'conservation'    => 0
		),
		'user' => array(
			'racine'          => '_DIR_VAR',
			'sous_dossier'    => false,
			'nom_prefixe'     => 'user',
			'nom_obligatoire' => array('liste'),
			'nom_facultatif'  => array(),
			'extension'       => '.json',
			'securisation'    => false,
			'serialisation'   => false,
			'decodage'        => true,
			'separateur'      => '_',
			'conservation'    => 0
		),
	);

	return $configuration;
}
