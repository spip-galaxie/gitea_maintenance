<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// fonction pour le pipeline, n'a rien a effectuer
function omgitea_autoriser() {
}

/**
 * Autorisation minimale d'accès à toutes les pages du plugin Contrib.
 * Par défaut, seuls les administrateurs complets sont autorisés à utiliser le plugin.
 * Cette autorisation est à la base de la plupart des autres autorisations du plugin.
 *
 * @param $faire
 * @param $type
 * @param $id
 * @param $qui
 * @param $options
 *
 * @return bool
 */
function autoriser_omgitea_dist($faire, $type, $id, $qui, $options) {
	return autoriser('defaut');
}

/**
 * Autorisation d'accès à la page de configuration du plugin (page=configurer_omgitea).
 * Par défaut, seuls les webmestres sont autorisés à modifier la configuration du plugin.
 *
 * @param $faire
 * @param $type
 * @param $id
 * @param $qui
 * @param $options
 *
 * @return bool
 */
function autoriser_omgitea_configurer_dist($faire, $type, $id, $qui, $options) {
	return
		autoriser('omgitea')
		and autoriser('webmestre');
}
