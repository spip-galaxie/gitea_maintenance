<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return mixed|string
 */
function omgitea_orga_repos($id_controle, $id_auteur, $options) {

	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// Récupération de la liste des organisations
	// -- construction de l'url de la requête de récupération des organisations
	$parametres = array(
		'admin',
		'orgs'
	);
	include_spip('services/gitea');
	$url = gitea_construire_url($parametres);

	// -- requête, gestion des erreurs et traitement des organisations
	$reponse = gitea_requeter($url);
	if (!empty($reponse['erreur'])) {
		$erreur = $reponse['erreur'];
	} else {
		include_spip('inc/flock');
		$orgas = $reponse['donnees'];
		foreach ($orgas as $_orga) {
			// Récupération des repos d'une organisation
			$parametres = array(
				'orgs',
				$_orga['username'],
				'repos'
			);
			$url = gitea_construire_url($parametres);
			$reponse = gitea_requeter($url);
			if (!empty($reponse['erreur'])) {
				$erreur = $reponse['erreur'];
				break;
			} else {
				// Extraction des repos et classement dans l'ordre alphabétique
				$repos = $reponse['donnees'];
				$repos = array_column($repos, null, 'name');
				ksort($repos);

				$contenu = '';
				foreach ($repos as $_repo) {
					$contenu .= $_repo['full_name'] . "\n";
				}

				// Création du fichier des repos de l'organisation
				// -- Initialisation de l'identifiant du cache
				$cache = array(
					'organisation' => $_orga['username']
				);
				// -- Ecriture du cache
				include_spip('inc/ezcache_cache');
				cache_ecrire('omgitea', 'repo', $cache, $contenu);
			}
		}
	}

	return $erreur;
}

/**
 * @param int   $id_controle
 * @param int   $id_auteur
 * @param array $options
 *
 * @return mixed|string
 */
function omgitea_user_recent($id_controle, $id_auteur, $options) {

	// Initialisation de l'erreur à chaine vide soit 'aucune erreur'.
	$erreur = '';

	// Construction de l'url de la requête de récupération de tous les users de la forge
	include_spip('services/gitea');
	$parametres = array(
		'admin',
		'users'
	);
	$url = gitea_construire_url($parametres);

	// Requête, gestion des erreurs et traitement des users
	$reponse = gitea_requeter($url);
	if (!empty($reponse['erreur'])) {
		$erreur = $reponse['erreur'];
	} else {
		// On retraite le tableau de façon à pouvoir classer les users du plus récent au plus ancien.
		// Pour cela on utilise l'id.
		$users = array_column($reponse['donnees'], null, 'id');
		krsort($users);

		// On extrait les n premiers users du tableau qui sont donc les plus récents
		$users_recents = array_slice($users, 0, intval($options['nb_users']));

		// On passe les deux tableau users n chaine JSON avant la mise en cache
		$contenu = json_encode($users);
		$contenu_recents = json_encode($users_recents);

		// Création des fichiers cache pour chaque contenu
		// -- Cache de tous les users
		include_spip('inc/ezcache_cache');
		$cache = array(
			'liste' => 'all'
		);
		cache_ecrire('omgitea', 'user', $cache, $contenu);
		// -- Cache des users récents
		$cache['liste'] = 'last';
		cache_ecrire('omgitea', 'user', $cache, $contenu_recents);
	}

	return $erreur;
}
